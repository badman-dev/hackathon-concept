# Uplift

## Visual Concepts

![Concept A](concepts/598b842696a71dcbdd6684839f253f66.png "Concept A")

- Notes
  - Replacement the music slider and with image/sharing capabilities
  - Reuse Category concepts

![Concept B](concepts/ulesson.png "Concept B")

- Notes
  - Personalized Greeting
  - Ability to provide favorites/popular quotes or phrases
  - Ignore Solutions page.

## Images... where do get them?
We can scrape these apis, but we have provide credit of where these came from... 
or we can use their urls, but cache the fuck out of it to minimize the hits to their apis.

- [https://unsplash.com/documentation] (https://unsplash.com/documentation)
- [https://pixabay.com/api/docs/] (https://pixabay.com/api/docs/)
- [https://www.pexels.com/api/] (https://www.pexels.com/api/)

## Available API that is free that we can scrape:

- https://forismatic.com/api/1.0/

`id = 1`

```
{"quoteText":"Do not wait for leaders; do it alone, person to person. ", "quoteAuthor":"Mother Teresa ",
"senderName":"", "senderLink":"", "quoteLink":"http://forismatic.com/en/7e4890cc74/"}
```

`id = 457653`

```
{"quoteText":"When your desires are strong enough you will appear to possess superhuman powers to achieve. ",
"quoteAuthor":"Napoleon Hill ", "senderName":"", "senderLink":"", "quoteLink":"http://forismatic.com/en/b00298f624/"}
```
- Notes:
  - We need to categorize the data
  - We need to scrape this api, and change how the data is used, to avoid issues,
  - We would need to make an actual api for this
    - Create/Edit/Delete operations
    - Caching rules
    - Fall back solutions
    - API Error handling
    - Tokenize/protect the endpoint

![API in Postman](readmeImages/API.PNG "API in Postman")


## Tech Stack Possibilities

- React Native/Flutter/Native Script/Ionic and some sort of api support
- DB of any kind needed, IF!!!! account creation is used.
- Express/Koa/Hapi